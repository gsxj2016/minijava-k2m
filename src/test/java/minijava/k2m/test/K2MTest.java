package minijava.k2m.test;

import minijava.k2m.gen.KangaVisitor;
import minijava.k2m.gen.State;
import minijava.k2m.jtb.KangaParser;
import minijava.k2m.jtb.ParseException;
import minijava.k2m.jtb.syntaxtree.Node;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.stream.Collectors;

public class K2MTest {

    private final String kgiPath;
    private final String spimPath;
    private final String efPath;


    public K2MTest() {
        kgiPath = System.getProperty("k2m.test.path.kgi", "D:/Programming/MIPS/kgi-patched.jar");
        // Note:
        // This SPIM is modified to print only program output, not any extraneous notice.
        // Do not use original SPIM here!
        spimPath = System.getProperty("k2m.test.path.spim", "D:/Programming/MIPS/spim.exe");
        efPath = System.getProperty("k2m.test.path.ef", "D:/Programming/MIPS/exceptions.s");

        System.out.println("kgi Path: " + kgiPath);
        System.out.println("SPIM Path: " + spimPath);
        System.out.println("SPIM exception file Path: " + efPath);
    }

    private String genAsm(String resName) throws IOException, ParseException {
        try (InputStream in = getClass().getResourceAsStream(resName)) {
            Node root = new KangaParser(in).Goal();
            State state = new State();
            root.accept(new KangaVisitor(), state);
            state.generateUtilFunctions();
            return state.code.toString();
        }
    }

    private String getKanga(String resName) throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream(resName)))) {
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }

    private String waitForProcessOutput(Process process) throws IOException {
        BufferedReader stderr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String error = stderr.lines().collect(Collectors.joining("\n"));
        stderr.close();

        BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String output = stdout.lines().collect(Collectors.joining("\n"));
        stdout.close();

        while (process.isAlive()) {
            try {
                process.wait(5000);
                if (process.isAlive()) {
                    System.out.println("Process is still alive, killing...");
                    process.destroyForcibly();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (error.length() > 0) {
            System.out.println("## Error occurred:");
            System.out.println(error);
            return null;
        }
        if (output != null) {
            System.out.println("** output length: " + output.length());
        }
        return output;
    }

    private String runKgi(String code) throws IOException {
        System.out.println("** code length: " + code.length());
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-jar", kgiPath});
        PrintWriter writer = new PrintWriter(process.getOutputStream());
        writer.print(code);
        writer.close();

        return waitForProcessOutput(process);
    }

    private String runSpim(String code) throws IOException {
        System.out.println("** code length: " + code.length());
        File temp = File.createTempFile("test", ".s");
        System.out.println("** Temp file: " + temp.getAbsolutePath());
        PrintWriter writer = new PrintWriter(temp);
        writer.print(code);
        writer.close();

        Process process = Runtime.getRuntime().exec(
                new String[]{spimPath, "-ef", efPath, "-f", temp.getAbsolutePath()});
        process.getOutputStream().close();

        String output = waitForProcessOutput(process);
        if (!temp.delete()) {
            System.out.println("*** WARN: delete temp file failed.");
        }
        return output;
    }

    private void testFile(String resName) throws IOException, ParseException {
        System.out.print("Testing with file: ");
        System.out.println(resName);
        String kCode = getKanga(resName);
        String aCode = genAsm(resName);
        // kgi
        System.out.println("* Run kanga interpreter...");
        String ans = runKgi(kCode);
        Assert.assertNotNull(ans);
        // spim
        System.out.println("* Run SPIM...");
        String out = runSpim(aCode);
        Assert.assertNotNull(out);

        if (ans.equals(out)) {
            System.out.println("Output is identical.");
        } else {
            System.out.println("## OUTPUT");
            System.out.println(out);
            System.out.println("## EXPECTED");
            System.out.println(ans);
            System.out.println("########");
            Assert.fail();
        }
        System.out.println();
    }

    @Test
    public void uclaTestCases() throws IOException, ParseException {
        testFile("/test_ucla/QuickSort.kg");
        testFile("/test_ucla/MoreThan4.kg");
        testFile("/test_ucla/BinaryTree.kg");
        testFile("/test_ucla/BubbleSort.kg");
        testFile("/test_ucla/TreeVisitor.kg");
        testFile("/test_ucla/LinearSearch.kg");
        testFile("/test_ucla/LinkedList.kg");
        testFile("/test_ucla/Factorial.kg");
    }

    @Test
    public void oldTestCases() throws IOException, ParseException {
        testFile("/test_old/QuickSort.kg");
        testFile("/test_old/MoreThan4.kg");
        testFile("/test_old/BinaryTree.kg");
        testFile("/test_old/BubbleSort.kg");
        testFile("/test_old/TreeVisitor.kg");
        testFile("/test_old/LinearSearch.kg");
        testFile("/test_old/LinkedList.kg");
        testFile("/test_old/Factorial.kg");
        testFile("/test_old/1-PrintLiteral.kg");
        testFile("/test_old/2-Add.kg");
        testFile("/test_old/3-Call.kg");
        testFile("/test_old/4-Vars.kg");
        testFile("/test_old/5-OutOfBounds.kg");
        testFile("/test_old/array.kg");
        testFile("/test_old/arrayOut.kg");
        testFile("/test_old/arrayOut2.kg");
        testFile("/test_old/arrayRef.kg");
        testFile("/test_old/arrayNull.kg");
        testFile("/test_old/arrayFetchNull.kg");
        testFile("/test_old/arrayLenNull.kg");
        testFile("/test_old/arrayNeg.kg");
        testFile("/test_old/arrayNeg2.kg");
        testFile("/test_old/zeroLenArray.kg");
        testFile("/test_old/badArrayAlloc.kg");
        testFile("/test_old/nestedCall.kg");
        testFile("/test_old/emptyClass.kg");
        testFile("/test_old/calc.kg");
        testFile("/test_old/override.kg");
        testFile("/test_old/callNull.kg");
        testFile("/test_old/shortcut.kg");
        testFile("/test_old/evalSeq.kg");
        testFile("/test_old/loop.kg");
        testFile("/test_old/spill.kg");
        testFile("/test_old/callerSave.kg");
        testFile("/test_old/emptyFun.kg");
        testFile("/test_old/sameLabel.kg");
        testFile("/test_old/sameLabelUsed.kg");
        testFile("/test_old/unused.kg");
        testFile("/test_old/unusedLabel.kg");
        testFile("/test_old/moreCallerSave.kg");
        testFile("/test_old/muchSave.kg");
    }
}
