package minijava.k2m.gen;

public class State {
    public static final String FUNCTION_CALL = "CALL";

    private int stackFrameSize;
    private boolean needRestoringRa;
    private String targetReg = null;
    private String operandReg = null;
    private OpType opType = OpType.MOVE;

    public final StringBuilder code = new StringBuilder();

    public void generateCode(String codeText) {
        code.append("    ");
        generateNoIndentCode(codeText);
    }

    private void generateNoIndentCode(String codeText) {
        code.append(codeText);
        code.append(System.lineSeparator());
    }

    public void beginFunction(String name, int stackSlots) {
        generateCode(".text");
        generateCode(String.format(".globl %s", name));
        generateNoIndentCode(String.format("%s:", name));
        // allocate stack space
        generateCode(String.format("subu $sp, $sp, %d", 4 * (stackSlots + 1)));
        // save return address
        generateCode("sw $ra, ($sp)");

        stackFrameSize = stackSlots;
        needRestoringRa = false;
        targetReg = null;
    }

    public void endFunction() {
        if (needRestoringRa) {
            // restore return address if needed
            generateCode("lw $ra, ($sp)");
        }
        // deallocate stack space
        generateCode(String.format("addu $sp, $sp, %d", 4 * (stackFrameSize + 1)));
        // return!
        generateCode("jr $ra");
        generateNoIndentCode("");
    }

    public void setTargetReg(String reg) {
        ensureState(null == targetReg, "set target reg twice");
        targetReg = reg;
        operandReg = null;
        opType = OpType.MOVE;
    }

    public void setBinOp(String op, String reg) {
        ensureState(opType == OpType.MOVE, "set op twice");
        operandReg = reg;
        opType = OpType.fromOpString(op);
    }

    public void ensureTargetConsumed() {
        ensureState(null == targetReg, "target not used");
    }

    public void putRegValue(String reg) {
        ensureState(null != targetReg, "target reg not set");

        if (FUNCTION_CALL.equals(targetReg)) {
            ensureState(opType == OpType.MOVE, "binOp shouldn't be here");
            generateCode(String.format("jalr $%s", reg));
        } else {
            if (opType == OpType.MOVE) {
                generateCode(String.format("move $%s, $%s", targetReg, reg));
            } else {
                generateCode(String.format("%s $%s, $%s, $%s", opType.getInst(), targetReg, operandReg, reg));
            }
        }
        targetReg = null;
    }

    public void putImmediate(int value) {
        ensureState(null != targetReg, "target reg not set");
        ensureState(!FUNCTION_CALL.equals(targetReg), "cannot call immediate");

        if (opType == OpType.MOVE) {
            generateCode(String.format("li $%s, %d", targetReg, value));
        } else {
            generateCode(String.format("%s $%s, $%s, %d", opType.getInst(), targetReg, operandReg, value));
        }
        targetReg = null;
    }

    public void putLabel(String label) {
        if (null != targetReg) {
            // function address
            ensureState(opType == OpType.MOVE, "cannot use label as binOp operand");
            if (FUNCTION_CALL.equals(targetReg)) {
                generateCode(String.format("jal %s", label));
            } else {
                generateCode(String.format("la $%s, %s", targetReg, label));
            }
        } else {
            // label
            generateNoIndentCode(String.format("%s:", label));
        }
        targetReg = null;
    }

    public int getStackOffset(int spilledArg) {
        return 4 * (stackFrameSize - spilledArg);
    }

    public void setNeedRestoringRa() {
        needRestoringRa = true;
    }

    public String popTargetReg() {
        ensureState(null != targetReg, "target reg not set");
        ensureState(opType == OpType.MOVE, "binOp shouldn't be here");
        String reg = targetReg;
        targetReg = null;
        return reg;
    }

    public void generateUtilFunctions() {
        // _halloc
        generateCode(".text");
        generateCode(".globl _halloc");
        generateNoIndentCode("_halloc:");
        generateCode("li $v0, 9");
        generateCode("syscall");
        generateCode("jr $ra");
        generateNoIndentCode("");
        // _print
        generateCode(".text");
        generateCode(".globl _print");
        generateNoIndentCode("_print:");
        generateCode("li $v0, 1");
        generateCode("syscall");
        generateCode("la $a0, _newl");
        generateCode("li $v0, 4");
        generateCode("syscall");
        generateCode("jr $ra");
        generateNoIndentCode("");
        // _error
        generateCode(".text");
        generateCode(".globl _error");
        generateNoIndentCode("_error:");
        generateCode("la $a0, _str_er");
        generateCode("li $v0, 4");
        generateCode("syscall");
        generateCode("li $v0, 10");
        generateCode("syscall");
        generateCode("jr $ra");
        generateNoIndentCode("");
        // new line
        generateCode(".data");
        generateNoIndentCode("_newl:");
        generateCode(".asciiz \"\\n\"");
        generateNoIndentCode("");
        // error text
        generateCode(".data");
        generateNoIndentCode("_str_er:");
        generateCode(".asciiz \"ERROR\\n\"");
        generateNoIndentCode("");
    }

    private static void ensureState(boolean cond, String msg) {
        if (!cond) throw new InternalCompilerError(msg);
    }
}
