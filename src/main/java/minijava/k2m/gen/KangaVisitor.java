package minijava.k2m.gen;

import minijava.k2m.jtb.syntaxtree.*;
import minijava.k2m.jtb.visitor.GJVoidDepthFirst;

public class KangaVisitor extends GJVoidDepthFirst<State> {
    /**
     * f0 -> "MAIN"
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> "["
     * f5 -> IntegerLiteral()
     * f6 -> "]"
     * f7 -> "["
     * f8 -> IntegerLiteral()
     * f9 -> "]"
     * f10 -> StmtList()
     * f11 -> "END"
     * f12 -> ( Procedure() )*
     * f13 -> <EOF>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Goal n, State argu) {
        // main function
        int slot = Integer.parseInt(n.f5.f0.tokenImage);
        argu.beginFunction("main", slot);
        n.f10.accept(this, argu);
        argu.endFunction();

        // other functions
        n.f12.accept(this, argu);
    }

    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> "["
     * f5 -> IntegerLiteral()
     * f6 -> "]"
     * f7 -> "["
     * f8 -> IntegerLiteral()
     * f9 -> "]"
     * f10 -> StmtList()
     * f11 -> "END"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Procedure n, State argu) {
        // function
        String fn = n.f0.f0.tokenImage;
        int slot = Integer.parseInt(n.f5.f0.tokenImage);
        argu.beginFunction(fn, slot);
        n.f10.accept(this, argu);
        argu.endFunction();
    }

    /**
     * f0 -> "NOOP"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(NoOpStmt n, State argu) {
        // noop
    }

    /**
     * f0 -> "ERROR"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ErrorStmt n, State argu) {
        argu.setNeedRestoringRa();
        argu.generateCode("jal _error");
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Reg()
     * f2 -> Label()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(CJumpStmt n, State argu) {
        String reg = n.f1.f0.choice.toString();
        String target = n.f2.f0.tokenImage;
        argu.generateCode(String.format("bne $%s, 1, %s", reg, target));
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(JumpStmt n, State argu) {
        String target = n.f1.f0.tokenImage;
        argu.generateCode(String.format("j %s", target));
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Reg()
     * f2 -> IntegerLiteral()
     * f3 -> Reg()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HStoreStmt n, State argu) {
        String addr = n.f1.f0.choice.toString();
        int offset = Integer.parseInt(n.f2.f0.tokenImage);
        String reg = n.f3.f0.choice.toString();
        argu.generateCode(String.format("sw $%s, %d($%s)", reg, offset, addr));
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Reg()
     * f2 -> Reg()
     * f3 -> IntegerLiteral()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HLoadStmt n, State argu) {
        String reg = n.f1.f0.choice.toString();
        String addr = n.f2.f0.choice.toString();
        int offset = Integer.parseInt(n.f3.f0.tokenImage);
        argu.generateCode(String.format("lw $%s, %d($%s)", reg, offset, addr));
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Reg()
     * f2 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MoveStmt n, State argu) {
        String reg = n.f1.f0.choice.toString();
        argu.setTargetReg(reg);
        n.f2.accept(this, argu);
        argu.ensureTargetConsumed();
    }

    /**
     * f0 -> "PRINT"
     * f1 -> SimpleExp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PrintStmt n, State argu) {
        argu.setNeedRestoringRa();
        argu.setTargetReg("a0");
        n.f1.accept(this, argu);
        argu.ensureTargetConsumed();
        argu.generateCode("jal _print");
    }

    /**
     * f0 -> "ALOAD"
     * f1 -> Reg()
     * f2 -> SpilledArg()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ALoadStmt n, State argu) {
        String reg = n.f1.f0.choice.toString();
        int slot = Integer.parseInt(n.f2.f1.f0.tokenImage);
        int offset = argu.getStackOffset(slot);
        argu.generateCode(String.format("lw $%s, %d($sp)", reg, offset));
    }

    /**
     * f0 -> "ASTORE"
     * f1 -> SpilledArg()
     * f2 -> Reg()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(AStoreStmt n, State argu) {
        String reg = n.f2.f0.choice.toString();
        int slot = Integer.parseInt(n.f1.f1.f0.tokenImage);
        int offset = argu.getStackOffset(slot);
        argu.generateCode(String.format("sw $%s, %d($sp)", reg, offset));
    }

    /**
     * f0 -> "PASSARG"
     * f1 -> IntegerLiteral()
     * f2 -> Reg()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PassArgStmt n, State argu) {
        String reg = n.f2.f0.choice.toString();
        int arg = Integer.parseInt(n.f1.f0.tokenImage);
        argu.generateCode(String.format("sw $%s, %d($sp)", reg, -4 * arg));
    }

    /**
     * f0 -> "CALL"
     * f1 -> SimpleExp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(CallStmt n, State argu) {
        argu.setNeedRestoringRa();
        argu.setTargetReg(State.FUNCTION_CALL);
        n.f1.accept(this, argu);
        argu.ensureTargetConsumed();
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> SimpleExp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HAllocate n, State argu) {
        argu.setNeedRestoringRa();
        String reg = argu.popTargetReg();
        // argument
        argu.setTargetReg("a0");
        n.f1.accept(this, argu);
        argu.ensureTargetConsumed();
        // do allocation
        argu.generateCode("jal _halloc");
        // write back
        argu.setTargetReg(reg);
        argu.putRegValue("v0");
        argu.ensureTargetConsumed();
    }

    /**
     * f0 -> Operator()
     * f1 -> Reg()
     * f2 -> SimpleExp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(BinOp n, State argu) {
        String op = n.f0.f0.choice.toString();
        String reg = n.f1.f0.choice.toString();
        argu.setBinOp(op, reg);
        n.f2.accept(this, argu);
    }

    /**
     * f0 -> "a0"
     * | "a1"
     * | "a2"
     * | "a3"
     * | "t0"
     * | "t1"
     * | "t2"
     * | "t3"
     * | "t4"
     * | "t5"
     * | "t6"
     * | "t7"
     * | "s0"
     * | "s1"
     * | "s2"
     * | "s3"
     * | "s4"
     * | "s5"
     * | "s6"
     * | "s7"
     * | "t8"
     * | "t9"
     * | "v0"
     * | "v1"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Reg n, State argu) {
        argu.putRegValue(n.f0.choice.toString());
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IntegerLiteral n, State argu) {
        argu.putImmediate(Integer.parseInt(n.f0.tokenImage));
    }

    /**
     * f0 -> <IDENTIFIER>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Label n, State argu) {
        argu.putLabel(n.f0.tokenImage);
    }
}
