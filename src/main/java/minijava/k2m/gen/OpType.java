package minijava.k2m.gen;

public enum OpType {
    MOVE(null),
    ADD("addu"),
    SUB("subu"),
    MUL("mul"),
    LT("slt");

    private final String inst;

    OpType(String inst) {
        this.inst = inst;
    }

    public String getInst() {
        if (null == inst) {
            throw new InternalCompilerError("unspecified inst");
        }
        return inst;
    }

    public static OpType fromOpString(String op) {
        switch (op) {
            case "PLUS":
                return ADD;
            case "MINUS":
                return SUB;
            case "TIMES":
                return MUL;
            case "LT":
                return LT;
            default:
                throw new InternalCompilerError("bad op str");
        }
    }
}
