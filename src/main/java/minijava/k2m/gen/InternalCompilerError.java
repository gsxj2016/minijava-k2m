package minijava.k2m.gen;

public class InternalCompilerError extends Error {
    public InternalCompilerError(String message) {
        super("[BUG] " + message);
    }
}
