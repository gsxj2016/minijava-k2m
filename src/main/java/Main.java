import minijava.k2m.gen.KangaVisitor;
import minijava.k2m.gen.State;
import minijava.k2m.jtb.KangaParser;
import minijava.k2m.jtb.ParseException;
import minijava.k2m.jtb.Token;
import minijava.k2m.jtb.syntaxtree.Node;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Error: No input file.");
            System.exit(2);
        }

        try (InputStream in = new FileInputStream(args[0])) {
            Node root = new KangaParser(in).Goal();
            State state = new State();
            root.accept(new KangaVisitor(), state);
            state.generateUtilFunctions();
            System.out.print(state.code.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (ParseException e) {
            Token token = e.currentToken;
            System.err.printf("Syntax error at %d:%d.\n", token.beginLine, token.beginColumn);
            System.exit(1);
        }
    }
}
